import { InternetConnectionService } from "../shared/services/internet-connection.service";
import { trigger, state, transition, style, animate } from '@angular/animations';
import { Feedback, FeedbackType, FeedbackPosition } from "nativescript-feedback";
import * as appSettings from "tns-core-modules/application-settings";
import { Component, NgZone, OnInit, ViewChild} from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { ApiService } from "../shared/services/api.service";
import { Page } from "tns-core-modules/ui/page";
import { Color } from "tns-core-modules/color";
const CryptoJS = require("crypto-js");

@Component({
    selector: "Register",
    moduleId: module.id,
    templateUrl: "./register.component.html",
    styleUrls: ["./register.component.css"],
    animations: []
})

export class RegisterComponent implements OnInit{
    connection$;
    user$;
    
    @ViewChild("password",{ static: false }) password: any;


    public _dataRoles: any;
    public _dataRegistro: { 
                            email: string, 
                            password: string, 
                            name: string, 
                            last_name: string,
                            rol: string 
                        };
    
    private feedback: Feedback;
    
    public dataUser: any;
    public connectionStatus: boolean;
    public isLoading: boolean;
    public isPasswordSize: boolean;

    constructor(
        private _internetConnection: InternetConnectionService,
        private _routerExtensions: RouterExtensions,
        private _apiService: ApiService,
        private _ngZone: NgZone,
        private page: Page) {

        this.feedback = new Feedback()
        this.isPasswordSize = true;
        this.isLoading = false;
    }


    ngOnInit() {
        this._dataRegistro = { email: '', password: '', name: '', last_name: '', rol: '' };
     
        this.connection$ = this._internetConnection.connectionStatus$.subscribe(data => {
            this.connectionStatus = data.valueOf();
        });
        
        this.user$ = this._apiService.User$.subscribe(value => {
            this.dataUser = value;
        })
    }

    ngAfterViewInit(): void {
        this._apiService.apiForGetRoles().then((res: any)=>{

                if (res.status != "success") {
                    this.showWarning("No hemos podido obtener los roles disponibles.");
                    return;
                }

                this._dataRoles = res.data; 
                console.dir(res.data);
        })
    }

    ngOnDestroy(): void {
        if (this.connection$){
            this.connection$.unsubscribe();
        }
        if (this.user$){
            this.user$.unsubscribe();
        }
    }
    
    public registerWithEmail() {
        console.dir("@registerWithEmail");

        if(this.isLoading || !this.connectionStatus) return;
        this.isLoading = true;

        let email, password, type, uid, token, nombre, apellido;
        
        email = this._dataRegistro.email ? this._dataRegistro.email : '';
        password = this._dataRegistro.password ? this._dataRegistro.password : '';

        nombre = this._dataRegistro.name ? this._dataRegistro.name : '';
        apellido = this._dataRegistro.last_name ? this._dataRegistro.last_name : '';
        

        token = appSettings.getString('token') ? appSettings.getString('token') : null;

        if (email == '' || !email) {
           this.showWarning("Ingrese un email valido para crear tu cuenta.");
           this.isLoading = false;
           return;
        }


        if (nombre == '' || !nombre || apellido == '' || !apellido) {
           this.showWarning("Ingrese un nombre y apellido valido para registrate.");
           this.isLoading = false;
           return;
        }

        if (!token) {
            if (password == '' || !password) {
               this.showWarning("Ingrese una contraseña valida para registrarte.");
               this.isLoading = false;
               return;
            }   
        }
          
        this.isLoading = true;

        let encrypted = CryptoJS.SHA256(this._dataRegistro.password).toString();
        
        this.dataUser.last_name = this._dataRegistro.last_name;
        this.dataUser.name = this._dataRegistro.name;
        this.dataUser.rol = this._dataRegistro.rol;
        
        this.dataUser.email = this._dataRegistro.email;
        this.dataUser.password = encrypted;
        
        this._apiService.firestoreAdd('users', this.dataUser).then((res: any)=>{

              if (res.status != 'success'){
                  this.isLoading = false;
                  this.showWarning(res.message);
                  return;
              }

              this.isLoading = false;
              console.dir(res);
                 
              this.showSuccess("Usuario cargado correctamente. Recuerda que deberan autorizar tu usuario para poder operar.");
              
              this.refreshDataAndToken();

              setTimeout(()=>{
                this.navigateTo('profile')
              }, 1000)

          })

    }

    public selectRol(item){
        this._dataRegistro.rol = item;
    }


    /* Refresh data */

    public refreshDataAndToken(){
        console.dir("@refreshDataAndToken");

        //set params appSetting
        appSettings.setString('user_id',String(this.dataUser.id))       
        appSettings.setString('email', this.dataUser.email)
        // appSettings.setString('password', this.dataUser.password)   
        
        //set observable
        this._apiService.setUser(this.dataUser);
        
        //set topics for push
        // const firebase = require("nativescript-plugin-firebase");
        // firebase.subscribeToTopic("user_"+this.dataUser.id).then(() => console.log("Subscribed to user_"+this.dataUser.id));
    }

    /* Feedbacks */

    private showWarning(message){
        this.feedback.warning({
              message: message,
              position: FeedbackPosition.Top,
              type: FeedbackType.Warning,
              backgroundColor: new Color("#df4a32"),
        });
    }


    private showSuccess(message){
        this.feedback.warning({
              message: message,
              position: FeedbackPosition.Top,
              type: FeedbackType.Success,
              backgroundColor: new Color("#95ba65"),
        });
    }
  

    /* Password's */

    public writePassword(args) {
        let textfield = args.object;
        let value = textfield.text;
        let size = value.length;
        
        if (size == 6) {
            this.isPasswordSize = true;
            return;
        }

        if (size > 1 && size < 6) {
            this.isPasswordSize = false;
        }
    }

    public toggleShow(args) {
        let label = args.object;

        this.password.nativeElement.secure = !this.password.nativeElement.secure;
        this.password.nativeElement.class = "form-input";

        label.color = (this.password.nativeElement.secure) ? '#dadada' : '#FB665B';
    }
    
  /* Navigation's */
    
    public navigateTo(page) {
        console.dir('@navigateTo', page);
        if(this.isLoading || !this.connectionStatus) return;
        this._ngZone.run(() => {
            this._routerExtensions.navigate([page], {
                    clearHistory: true,
                    animated: true,
                    transition: {
                        name: "slideLeft",
                        duration: 350,
                        curve: "ease"
                    }
            })
        })
    }

}