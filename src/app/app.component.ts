import { AndroidApplication, AndroidActivityBackPressedEventData } from "tns-core-modules/application";
import { registerElement } from "nativescript-angular/element-registry";
import { RouterExtensions } from "nativescript-angular/router";
import * as application from "tns-core-modules/application";
import { Component, OnInit, NgZone } from "@angular/core";
import { NavigationEnd, Router } from "@angular/router";
import { isAndroid } from "tns-core-modules/platform";

registerElement("PullToRefresh", () => require("@nstudio/nativescript-pulltorefresh").PullToRefresh);
registerElement("Mapbox", () => require("nativescript-mapbox").MapboxView);

@Component({
    moduleId: module.id,
    selector: "ns-app",
    templateUrl: "app.component.html"
})

export class AppComponent{

	constructor(
    private _router: Router, 
    private _ngZone: NgZone, 
    private _routerExtensions: RouterExtensions) {
		
	}

	ngOnInit(){

          if (isAndroid) {
            application.android.on(AndroidApplication.activityBackPressedEvent, (data: AndroidActivityBackPressedEventData) => {        

	        	if (this._router.isActive("/register", false)) {
                  data.cancel = true; // prevents
                this.navigateTo('slideLeft', 'login');
	           	}

	            if (this._router.isActive("/set-direccion", false)) {
                  data.cancel = true; // prevents
                  this.navigateTo('slideLeft', 'profile');
	            }

            
             	if (this._router.isActive("/categorias", false) || this._router.isActive("/notificaciones", false) || this._router.isActive("/nuevo-servicio", false)) {
                  data.cancel = true; // prevents
                  this.navigateTo('slideLeft', 'home');
            	}

          });
        }
	}

    navigateTo(navTransition, navPage): void {

        this._ngZone.run(()=>{

          this._routerExtensions.navigate([navPage],{
              clearHistory: false,
              animated: true,
              transition: {
                  name: navTransition,
                  duration: 350,
                  curve: "ease"
              }
          });

        }) 
    }
}

