import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";

import { NotificacionesRoutingModule } from "./notificaciones-routing.module";
import { NotificacionesComponent } from "./notificaciones.component";

import { SharedModule } from "../shared/shared.module"

@NgModule({
    imports: [
        NativeScriptCommonModule,
        NotificacionesRoutingModule,
        NativeScriptUIListViewModule,
        SharedModule
    ],
    declarations: [
        NotificacionesComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class NotificacionesModule { }
