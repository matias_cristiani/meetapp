import { Component, ElementRef, OnInit, AfterContentInit, OnDestroy, ViewChild, NgZone } from "@angular/core";
import { InternetConnectionService } from "../shared/services/internet-connection.service";
import { trigger, state, transition, style, animate } from '@angular/animations';
import { Feedback, FeedbackType, FeedbackPosition } from "nativescript-feedback";
import { RadListView, ListViewEventData } from "nativescript-ui-listview";
import { ObservableArray } from "tns-core-modules/data/observable-array";
import * as appSettings from "tns-core-modules/application-settings";
import { RouterExtensions } from "nativescript-angular/router";
import { Observable } from "tns-core-modules/data/observable";
import { ApiService } from "../shared/services/api.service";
import { alert } from "tns-core-modules/ui/dialogs";
import { isIOS } from "tns-core-modules/platform";
import { Page } from "tns-core-modules/ui/page";
import { Color } from "tns-core-modules/color";

declare var UIView, NSMutableArray, NSIndexPath;

@Component({
    selector: "Notificaciones",
    moduleId: module.id,
    templateUrl: "./notificaciones.component.html",
    styleUrls: ["./notificaciones.component.css"],
    animations: []
})

export class NotificacionesComponent implements OnInit, AfterContentInit, OnDestroy {
    connection$;
    user$;

    public dataNotification: ObservableArray<any>;    
    public dataUser: any;

    public feedback: any;

    public isLoading: boolean; 
    public connectionStatus: boolean;

    renderView = false;
    renderViewTimeout: any;

    constructor(
        private _internetConnection: InternetConnectionService,
        private _routerExtensions: RouterExtensions,
        private _apiService: ApiService,
        private _ngZone: NgZone,
        private page: Page) {
        
        this.page.actionBarHidden = false;
        this.page.backgroundSpanUnderStatusBar = false;
        this.page.className = "page-notificaciones-container";
        this.page.statusBarStyle = "dark";

        this.feedback = new Feedback()
        this.dataNotification = new ObservableArray();
        
        this.connectionStatus = true;
        this.isLoading = false;
    }
  
  
    ngOnInit(): void {
        this.isLoading = true;
        
        this.connection$ = this._internetConnection.connectionStatus$.subscribe(data => {
            this.connectionStatus = data.valueOf();
        });

    }
    
    ngAfterContentInit() {
        this.renderViewTimeout = setTimeout(() => {
            this.renderView = true;
            this.initializeNotifications();
        }, 500);
    }

    ngOnDestroy(): void {
        if (this.connection$){
            this.connection$.unsubscribe();
        }
    
        if (this.user$) {
            this.user$.unsubscribe();
        }
        
        clearTimeout(this.renderViewTimeout);
    }

    public initializeNotifications(){
        console.dir("@initializeNotifications");
        this.isLoading = false;
    }

   

    public onItemTap(event: ListViewEventData) {
        const listView = event.object, rowIndex = event.index, dataItem = event.view.bindingContext;
              
        let options = {
                title: dataItem.data.title ? dataItem.data.title : 'Notificacion',
                message: dataItem.data.body,
                okButtonText: "OK"
                };

                alert(options).then(() => {
                    dataItem.read_at = { date: new Date()};
                });
 
        if (isIOS) {
                  var indexPaths = NSMutableArray.new();
                  indexPaths.addObject(NSIndexPath.indexPathForRowInSection(rowIndex, event.groupIndex));
                  listView.ios.reloadItemsAtIndexPaths(indexPaths);
        }else{
              listView.androidListView.getAdapter().notifyItemChanged(rowIndex);
        }
    }

    
    public updateNotification(id) {

    }

    /* feedbacks */
    
    private showWarning(message){
        this.feedback.warning({
              message: message,
              position: FeedbackPosition.Top,
              type: FeedbackType.Warning,
              backgroundColor: new Color("#df4a32"),
        });
    }

    private showSuccess(message){
        this.feedback.warning({
              message: message,
              position: FeedbackPosition.Top,
              type: FeedbackType.Success,
              backgroundColor: new Color("#95ba65"),
        });
    }
    
    /* navigation */

    public navigateTo(page) {
        console.dir("@navigateTo");
        if(this.isLoading || !this.connectionStatus) return;

        this._ngZone.run(() => {
            this._routerExtensions.navigate([page], {
                    clearHistory: true,
                    animated: true,
                    transition: {
                        name: "slideBottom",
                        duration: 200,
                        curve: "ease"
                    }
            })
        })
    }

}
