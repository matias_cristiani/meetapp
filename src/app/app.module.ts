import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptAnimationsModule } from "nativescript-angular/animations";

/* Http */
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";

/* Guard */
import { LoggedInLazyLoadGuard } from "./logged-in-lazy-load.guard";

/* Services */
import { InternetConnectionService } from "./shared/services/internet-connection.service"
import { GeolocationService } from "./shared/services/geolocation-service"
import { ApiService } from "./shared/services/api.service"

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";

@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        NativeScriptAnimationsModule,
        NativeScriptHttpClientModule,
        NativeScriptModule,
        AppRoutingModule
    ],
    declarations: [
        AppComponent
    ],
    providers: [
        ApiService,
        GeolocationService,
        LoggedInLazyLoadGuard,
        InternetConnectionService
    ],
    exports: [
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class AppModule { }
