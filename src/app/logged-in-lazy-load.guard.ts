import { Injectable } from "@angular/core";
import { CanLoad } from "@angular/router";
import { RouterExtensions } from "nativescript-angular/router";
import * as appSettings from "tns-core-modules/application-settings";

@Injectable()
export class LoggedInLazyLoadGuard implements CanLoad {
	private user_id: string;
    
    constructor(private _routerExtensions: RouterExtensions) { }

    canLoad(): boolean {
        
        if (appSettings.getString('user_id')) {
         this.user_id = appSettings.getString('user_id');
        }

        if (!this.user_id) {
             this._routerExtensions.navigate(["login"], { clearHistory: true });
        }

        return true;
    }
}
