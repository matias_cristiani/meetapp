import { Component, ElementRef, Input, ViewChild } from "@angular/core";
import { Color } from "tns-core-modules/color";

@Component({
    selector: "ThreeLoading",
    moduleId: module.id,
    template: `
    <Label 
    	[visibility]="(isLoading) ? 'visible': 'collapsed'"
    	verticalAlignment="center"
    	horizontalAlignment="center" 
        color="#674ea7"
    	class="pulse-infinite" 
    	textWrap="true">
    	<FormattedString>
	    	<Span class="fas"  fontSize="30" text="meet"></Span>
	    	<Span class="futura-bold" marginLeft="-10"  fontSize="30" text="APP"></Span>
    	</FormattedString>
    </Label>
        
    `
})
export class ThreeLoading {
    @Input() isLoading: boolean;
}
