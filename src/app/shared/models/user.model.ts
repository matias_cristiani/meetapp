
import { Properties } from "./properties.model";

export class User { 
	 id: number;
	 name: string;
	 last_name: string;
	 status: any;
	 picture: any;
	 picture_thumb: any;
	 address: any;
	 authorization: any;
	 email: string;
	 password: string;
	 properties: Properties;
	 
	 hasEmail() {
	    return this.email != '';
	 }

}
