export class Properties { 
	role_key: "customer";
	notification_preferences?: { token : string }; 
    lang_code?: string;
	cuit?: string;
	razon_social?: string;
	tipo?: string;
	instragram?: string;
	phone_number?: string;
	phone_number_aux?: string;
}
