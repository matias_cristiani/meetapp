import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

/* componentes */
import { ThreeLoading } from "./components/three-loading/three-loading.component";
import { ConnectionStatus } from "./components/connection-status/connection-status.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        ],
    declarations: [
        ThreeLoading,
        ConnectionStatus
        ],
    exports: [
        ThreeLoading,
        ConnectionStatus
    ],
    providers:[],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class SharedModule { }
