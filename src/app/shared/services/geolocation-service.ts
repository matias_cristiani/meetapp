import * as appSettings from "tns-core-modules/application-settings";
import { BehaviorSubject } from "rxjs/internal/BehaviorSubject";
import * as geolocation from "nativescript-geolocation";
import { Observable } from "rxjs/internal/Observable";
import { Accuracy } from "tns-core-modules/ui/enums";
import { Subject } from "rxjs/internal/Subject";
import { Injectable } from '@angular/core';

@Injectable()
export class GeolocationService {

    private _geolocationStatusSubject$: BehaviorSubject<boolean>;
    private _positionSubject$: BehaviorSubject<any>;
    // locations = [];
    watchIds = [];

    constructor() {
        this._geolocationStatusSubject$ = new BehaviorSubject<boolean>(false);
        this._positionSubject$ = new BehaviorSubject<any>({latitude:0, longitude:0});
        
        geolocation.isEnabled().then((isEnabled) => {
            this._geolocationStatusSubject$ = new BehaviorSubject<boolean>(isEnabled);
            this.startMonitoring();
        })
    }

    get geolocationStatus$(): Observable<boolean> {
        return this._geolocationStatusSubject$.asObservable();
    }
    
    get geolocationPosition$(): Observable<any> {
        return this._positionSubject$.asObservable();
    }

    setPosition(loc){
      this._positionSubject$.next(loc);
    }

    public enableLocation() {
        geolocation.isEnabled().then((isEnabled) => {
            if (!isEnabled) {
                geolocation.enableLocationRequest(true, true).then(() => {
                    console.log("User Enabled Location Service");
                    this._geolocationStatusSubject$.next(true);
                }, (e) => {
                    console.log("Error: " + (e.message || e));
                }).catch(ex => {
                    console.log("Unable to Enable Location", ex);
                });
            }
        }, (e) => {
            console.log("Error: " + (e.message || e));
        });
    }

    public getCurrentLocation() {
        return new Promise((resolve, reject) => {  
        let that = this;
        geolocation.getCurrentLocation({
            desiredAccuracy: Accuracy.any,
            maximumAge: 5000,
            timeout: 10000
        }).then( (loc) => {
            resolve({ status:'success', data: loc });
        
        },  (e) => {
            resolve({ status: 'error', data: null });
            console.log("Error: " + (e.message || e));
        });
        })
    }

    public stopMonitoring() {
        let watchId = this.watchIds.pop();
        while (watchId != null) {
            geolocation.clearWatch(watchId);
            watchId = this.watchIds.pop();
        }
    }

    public startMonitoring() {
        try {
            let that = this;
            
            this.watchIds.push(geolocation.watchLocation( (loc) => {
                    if (loc) {
                        this.setPosition(loc);
                    }
            }, (e) => {
                    console.log("Error: " + e.message);
            },
            {
                desiredAccuracy: Accuracy.any,
                updateDistance: 1000, //mts
                updateTime: 300000, // 5'
                minimumUpdateTime: 150000, // min diference 2.5
                iosAllowsBackgroundLocationUpdates: false,
                iosPausesLocationUpdatesAutomatically: true
            })
            );

        } catch (ex) {
            console.log("Error: " + ex.message);
        }
    }
}