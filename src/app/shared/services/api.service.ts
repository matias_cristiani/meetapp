import * as appSettings from "tns-core-modules/application-settings";
import { BehaviorSubject } from "rxjs/internal/BehaviorSubject";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { firestore } from "nativescript-plugin-firebase";
import { Observable } from "rxjs/internal/Observable";
import { Subject } from "rxjs/internal/Subject";
import { Injectable, NgZone } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable()
export class ApiService {
    
    private response: {
      status: string,
      message: string,
      data: any
    };
    
    // private Meetups$: Observable<Array<any>>;

    private _dataUser: BehaviorSubject<any>;
    private _dataMeetups: BehaviorSubject<Array<any>>;
    private _dataConfig: BehaviorSubject<any>;

    constructor(
      private http: HttpClient,
      private _ngZone: NgZone) {
        
        this.response = { status: "success", message: "", data: null };
        
        this._dataMeetups = new BehaviorSubject<any>([]);

        this._dataUser = new BehaviorSubject<any>({
          id: 0,
          doc_number: 0,
          name: "",
          last_name: "",
          status: '0',
          avatar: null,
          address: null,
          address_str: null,
          rol: "",
          rol_str: "",
          isAdmin: false,
          phone_number: "",
          authorization: "",
          notification_token: "",
          email: appSettings.getString('email') ? appSettings.getString('email') : "",
          password: appSettings.getString('password') ? appSettings.getString('password') : "",
        });


        this._dataConfig = new BehaviorSubject<any>({
          mapbox:  appSettings.getString('mbox_ios_key') ? appSettings.getString('mbox_ios_key') : 'sk.eyJ1Ijoid2VibWVlbWJhIiwiYSI6ImNrMG8zNmo5MDA2ajUzbG1kczgydDlvYmIifQ.1acS_GD7SnLjOdD6Nv0tzw'
        })

        this.apiForGetMeetups();
    }

  /* OBSERVABLES */

  get Config$(): Observable<any> {
        return this._dataConfig.asObservable();
  }
   
  get User$(): Observable<any> {
        return this._dataUser.asObservable();
  }

  setUser(user){
    this._dataUser.next(user);
  }

  get Meetup$(): Observable<any> {
    return this._dataMeetups;
  }

  setMeetup(meetups){
    this._dataMeetups.next(meetups);
  }

  /* METHODS */

  firestoreQuery(collection, key, operator, value): any{
    let query = firestore.collection(collection).where(key, operator, value);
    return query.get();
  }

  firestoreAdd(collection: string, document: any): Promise<any> {
    return new Promise((resolve, reject) => {  
    firestore.collection(collection).add(document).then((docRef: firestore.DocumentReference) => {
          
          this.response.status = "success";
          this.response.message = "Creado exitosamente!";
          this.response.data = docRef.id;

          resolve(this.response)

        }).catch((err) => {

          this.response.status = "ERROR";
          this.response.message = "No hemos podido crear este documento!";
          this.response.data = err;

          reject(this.response)
          console.log("Adding Fido failed, error: " + err)
        });
    })
  }

  firestoreSet(collection: string, document_id: string, document: any, merged: boolean): Promise<any> {
    return new Promise((resolve, reject) => {  
    firestore.collection(collection).doc(document_id).set(document, {merge: merged}).then(() => {
          
          this.response.status = "success";
          this.response.message = "Actualizado correctamente!";
          this.response.data = document_id;

          resolve(this.response)
          
         }).catch((err) => {

          this.response.status = "ERROR";
          this.response.message = "No hemos podido crear este documento!";
          this.response.data = err;

          reject(this.response)
          console.log("Adding Fido failed, error: " + err)
        });
    })
  }

  firestoreGet(collection: string, path: string): Promise<any> {
    return new Promise((resolve, reject) => {  
      
    firestore.getDocument(collection, path).then((documentSnapshot: any) => {
            
          this.response.status = "success";
          this.response.message = "Actualizado correctamente!";
          this.response.data = documentSnapshot.data(documentSnapshot);

          resolve(this.response)
          
         }).catch((err) => {

          this.response.status = "ERROR";
          this.response.message = "No hemos podido crear este documento!";
          this.response.data = err;

          reject(this.response)
          console.log("Adding Fido failed, error: " + err)
        });
    })
  }



  /* API FOR REGISER */

  apiForGetRoles() {
    console.dir("@apiForGetRoles");
    return new Promise((resolve, reject) => {  
    
    firestore.collection("roles").get().then((querySnapshot: any) => {

           // console.dir(querySnapshot);
           
           if (querySnapshot && querySnapshot.docs.length == 0) {
             this.response.status = 'ERROR';
             this.response.message = 'No hemos encontrado roles';
             resolve(this.response);
           }

           console.dir(querySnapshot);
           this.response.data = [];
            
            querySnapshot.forEach((doc: any) => {
               let document = doc.data();
                   document.id = doc.id;
               this.response.data.push(document);               
            });
            
            this.response.status = "success";
            this.response.message = "Has obtenido "+this.response.data.length+' roles';

            resolve(this.response);

      }).catch((err: any)=>{
          console.dir(err);
          this.response.status = 'ERROR';
          this.response.message = 'Hubo un error al procesar la solicitud.';
          reject(this.response)
      });

    });
  }

  apiForRegister(formulario: any) {  
    return new Promise((resolve, reject) => {  
      
    })
  }
  
  /* API FOR LOGIN */
  apiForLogin(email: string, password: string = "", social: boolean) {
    return new Promise((resolve, reject) => {  
      
      email = email.toLowerCase();

      firestore.collection("users").where("email", "==", email).get().then((querySnapshot: any) => {

           // console.dir(querySnapshot);
           
           if (querySnapshot && querySnapshot.docs.length == 0) {
             this.response.status = 'ERROR';
             this.response.message = 'El email y/o contraseña ingresado es incorrecto.';
             resolve(this.response);
           }

           querySnapshot.forEach((doc: any) => {
               let document = doc.data();
                   document.id = doc.id;
                  
               

               if(social){
                 this.response.status = "success";
                 this.response.message = "Iniciado correctamente.";
                 this.response.data = document;

                 resolve(this.response);
               }


               if(String(document.status) == '0'){
                 this.response.status = "error";
                 this.response.message = "Tu usuario aun no ha sido autorizado.";
                 this.response.data = null;

                 resolve(this.response);
               }

               if (document.password == password) {

                 this.response.status = "success";
                 this.response.message = "Iniciado correctamente.";
                 this.response.data = document;

                 resolve(this.response);
               
               }else{
                 this.response.status = 'ERROR';
                 this.response.message = 'El email y/o contraseña ingresado es incorrecto.';
                 resolve(this.response);
               }
               
          });

      }).catch((err: any)=>{
          console.dir(err);
          this.response.status = 'ERROR';
          this.response.message = 'Hubo un error al procesar la solicitud.';
          reject(this.response)
      });

    }) 
  }


 /* API FOR HOME */

 public apiForGetWeather(coordinates: any) {
    return new Promise((resolve, reject) => {  

    let url = "https://weather.cit.api.here.com/weather/1.0/report.json?product=observation";
        url += "&latitude=" + coordinates.latitude + "&longitude=" + coordinates.longitude;
        url += "&app_id=devportal-demo-20180625&longitude=&app_code=9v2BkviRwi9Ot26kp2IysQ";
        url += "&one_observation=true&language=es-ES";
    
    this.http.get(url).subscribe((result: any) => {

               this.response.status = "success";
               this.response.message = "";
               this.response.data = result.observations.location[0];
               resolve(this.response);

        }, (err: any) => {
            this.response.status = 'ERROR';
            this.response.message = (err.error && err.error.message) ? (err.error.message) : 'Hubo un error al procesar la solicitud.';
            reject(this.response)
        });
    })
}

apiForGetMeetups() {
    console.dir("@apiForGetMeetups");
    
    this._dataMeetups = Observable.create(subscriber => {
      const colRef: firestore.CollectionReference = firestore.collection("meetups");
      colRef.onSnapshot((snapshot: firestore.QuerySnapshot) => {
        this._ngZone.run(() => {
         
         let meetups = [];
          
          snapshot.forEach(docSnap => {
            let doc = <any>docSnap.data();
                doc.id = docSnap.id;

                meetups.push(doc)
          })

          subscriber.next(meetups);
        });
      });
    });
}

/* API FOR NUEVO MEET */

apiForGetUsers() {
    console.dir("@apiForGetUsers");
    return new Promise((resolve, reject) => {  
    
    firestore.collection("users").get().then((querySnapshot: any) => {

           // console.dir(querySnapshot);
           
           if (querySnapshot && querySnapshot.docs.length == 0) {
             this.response.status = 'ERROR';
             this.response.message = 'No hemos encontrado usuarios';
             resolve(this.response);
           }

           console.dir(querySnapshot);
           this.response.data = [];
            
            querySnapshot.forEach((doc: any) => {
               let document = doc.data();
                   document.id = doc.id;
               this.response.data.push(document);               
            });
            
            this.response.status = "success";
            this.response.message = "Has obtenido "+this.response.data.length+' usuarios';

            resolve(this.response);

      }).catch((err: any)=>{
          console.dir(err);
          this.response.status = 'ERROR';
          this.response.message = 'Hubo un error al procesar la solicitud.';
          reject(this.response)
      });

    });
}

}