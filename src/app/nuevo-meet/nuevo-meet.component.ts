
import { Component, ElementRef, OnInit, AfterViewInit, OnDestroy, ViewChild, NgZone } from "@angular/core";
import { animate, state, style, transition, trigger } from "@angular/animations";
import { RouterExtensions } from "nativescript-angular/router";
import { isIOS, isAndroid } from "tns-core-modules/platform";
import { MapboxViewApi, Viewport as MapboxViewport } from "nativescript-mapbox";
import { InternetConnectionService } from "../shared/services/internet-connection.service";
import { Feedback, FeedbackType, FeedbackPosition } from "nativescript-feedback";
import { GooglePlacesAutocomplete } from 'nativescript-google-places-autocomplete';
import * as appSettings from "tns-core-modules/application-settings";
import { ApiService } from "../shared/services/api.service";
import { Page } from "tns-core-modules/ui/page";
import { Color } from "tns-core-modules/color";

@Component({
    selector: "NuevoMeet",
    moduleId: module.id,
    templateUrl: "./nuevo-meet.component.html",
    styleUrls: ["./nuevo-meet.component.css"],
    animations: []
})
export class NuevoMeetComponent implements OnInit, AfterViewInit, OnDestroy {
    connection$;
    user$;


    @ViewChild('pager', { static: true }) pager: any;
    @ViewChild("mapbox",{ static: false }) mapbox: any;
    
    public connectionStatus: boolean;
    public isLoading: boolean; 
    
    public dataUser: any;
    public dataMeetup: any;
    public dataUsers: any;

    public currentPagerIndex: number;
    private map: MapboxViewApi;
    private feedback: Feedback;

    constructor(
        private _apiService: ApiService,
        private _internetConnection: InternetConnectionService,
        private _routerExtensions: RouterExtensions,
        private _ngZone: NgZone,
        private page: Page) {
       
       this.dataMeetup = { 
                         title: '',
                         subject: '',
                         address: null,
                         date:'', 
                         owner_id: '',
                         participations_ids: []
        };

              
        this.page.actionBarHidden = false;
        this.page.backgroundSpanUnderStatusBar = false;
        this.page.className = "page-home-container";
        this.page.statusBarStyle = "dark";

        this.feedback = new Feedback()
        this.currentPagerIndex = 0;
        this.isLoading = false;

    }
  
  public nextPageAndLocation(args) { 
      console.dir("@setDirection")
      

       let markers = [
        {
            title: 'MEETUP',
            subtitle: this.dataMeetup.address.direccion,
            selected: true,
            lat: this.dataMeetup.address.lat,
            lng: this.dataMeetup.address.long
        }]
        

        this.map.addMarkers(markers)
        
        try{
          this.map.setCenter({
                lat: this.dataMeetup.address.lat,
                lng: this.dataMeetup.address.long,
                animated: true // default true
          })
        }catch(e){
          console.dir("error@setCenter")
        }
       
      setTimeout(()=>{
        this.nextPage(args)
      },600)
  }
  
  public submitMeetup(args) { 
 
    this.dataMeetup.owner_id = this.dataUser;
    
    console.dir(this.dataMeetup);
    
    this._apiService.firestoreAdd('addresses', this.dataMeetup.address).then((res_cero: any)=>{
      
      delete this.dataMeetup.address;

      this._apiService.firestoreAdd('meetups', this.dataMeetup).then((res_uno: any)=>{

                if (res_uno.status != 'success'){
                    this.isLoading = false;
                    this.showWarning(res_uno.message);
                    return;
                }

                this.isLoading = false;
               
                this.showSuccess("Meetup cargado correctamente.");

                this._apiService.firestoreSet('meetups', res_uno.data, {
                  address: res_cero.data
                }, true).then((res: any)=>{
              
                  setTimeout(()=>{
                    this.navigateTo('home')
                  }, 1000)

                })
      })

    })
  }

  resetPage() {
    console.dir("@prevPage")
    
      
    this.dataMeetup = { 
                         title: '',
                         subject: '',
                         address: null,
                         date:'20.2.2019 20:30', 
                         tipo: 'calendar', 
                         owner_id: '',
                         participations_ids: []
    };

    this.currentPagerIndex = 0;
  }

  prevPage(args = 1) {
    console.dir("@prevPage")
    let newIndex = this.currentPagerIndex - args;
    this.currentPagerIndex = newIndex;
  }

  nextPage(args) {
    console.dir("@nextPage")
    console.dir(this.dataMeetup);

    let newIndex = this.currentPagerIndex + args
    this.currentPagerIndex = newIndex;
  }
 
  ngOnInit(): void {
        this.connection$ = this._internetConnection.connectionStatus$.subscribe(data => {
            console.dir("this.connectionStatus",this.connectionStatus)
            this.connectionStatus = data.valueOf();
        });
         
         this.user$ = this._apiService.User$.subscribe((user) => {
           this.dataUser = user;  
        })

  }

  ngAfterViewInit(){
     
     this._apiService.apiForGetUsers().then((res: any)=>{
       if (res.status != "success") {
                    this.showWarning("No hemos podido obtener los usuarios disponibles");
                    return;
        }
        this.dataUsers = res.data;
        console.dir(res.data);

     })
  }

  ngOnDestroy(): void {
        if (this.connection$){
            this.connection$.unsubscribe();
        }
        if (this.user$){
            this.user$.unsubscribe();
        }
  }


  public onMapReady(args){
      console.dir("@onMapReady");
      let map = args.map;
      this.map = map;

      this.map.trackUser({
            mode: "NONE", // "NONE" | "FOLLOW" | "FOLLOW_WITH_HEADING" | "FOLLOW_WITH_COURSE"
            animated: true
      });      
  }


  /* feedbacks */
    
  showWarning(message){
        this.feedback.warning({
              message: message,
              position: FeedbackPosition.Top,
              type: FeedbackType.Warning,
              backgroundColor: new Color("#df4a32"),
        });
  }

  showSuccess(message){
        this.feedback.warning({
              message: message,
              position: FeedbackPosition.Top,
              type: FeedbackType.Success,
              backgroundColor: new Color("#95ba65"),
        });
  }



  /* navigations */
    
    public navigateTo(page, params = null) {
      this._ngZone.run(() => {
        
        let toPage = (params) ? [page].concat(params) : [page];
        console.dir(toPage);
        this._routerExtensions.navigate(toPage, {
          clearHistory: true,
          animated: true,
          transition: {
            name: "slideRight",
            duration: 200,
            curve: "ease"
          }
        });

      });
   }
}
