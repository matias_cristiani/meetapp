import { Component, ElementRef, Input, Output, EventEmitter, ViewChild } from "@angular/core";
import * as frame from "tns-core-modules/ui/frame";
import { Feedback, FeedbackType, FeedbackPosition } from "nativescript-feedback";
import { isIOS, isAndroid } from "tns-core-modules/platform";
import { Color } from "tns-core-modules/color";
import { TextField } from "tns-core-modules/ui/text-field";
import { ObservableArray } from "tns-core-modules/data/observable-array";
import { GooglePlacesAutocomplete } from 'nativescript-google-places-autocomplete';

@Component({
    selector: "paso-dos-component",
    templateUrl: 'paso-dos.component.html',
    moduleId: module.id,
    styles: [``],
    
})

export class PasoDosComponent {
    public isDirectionSelected: boolean;
    public _dataDirections: ObservableArray<any>;
    public _direction: any;

    private feedback: Feedback;
    private googlePlacesAutoComplete: GooglePlacesAutocomplete;

    @ViewChild("textField",{static: false}) textField: ElementRef;
    
    @Input() meetup: any;
    @Output() nextPage = new EventEmitter();
    @Output() backPage = new EventEmitter();

    constructor(
    ) {

      this.isDirectionSelected = false;

      this._dataDirections = new ObservableArray();
      

      this.feedback = new Feedback()
      this.googlePlacesAutoComplete = new GooglePlacesAutocomplete("AIzaSyB28j7uM5ZN0hbeBev-BNLna6g1HGaZB8I");


        this._direction = { 
                    direccion: "", 
                    placeid: "", 
                    observacion: "", 
                    selected: false,
                    lat: 0, 
                    long: 0, 
                  };

    }

    openFocusOnTextField() {
        this.textField.nativeElement.focus();
    }

    public onClear(args) {
        console.dir("@onClear");
        this.isDirectionSelected = false;
        this._dataDirections = new ObservableArray<any>();
    }

    public onTextChanged(args) {
        let textfield = <TextField>args.object;
        let value = textfield.text;
        let size = value.length;
        let reading = true;
        
        if (size==0) {
          // this.isDirectionSelected = false;
          // return;
        }
        // si la dire fue seleccionada, si es nulo, menor a 4 caracteres y/o es impar evito busqueda
        if ( this.isDirectionSelected || (!value) || (size <= 6) || (size%2 > 0 )) { return };
        
        if(isAndroid){
          textfield.android.clearFocus();
        }
        
        this._dataDirections = new ObservableArray<any>();

        this.googlePlacesAutoComplete.search(textfield.text, 'ar', 'geocode').then((placeResult: any) => {
   
           if (!placeResult) { return; }
              
              console.dir(placeResult.length);
              
              placeResult.forEach((place)=>{
                  console.dir(place);

                  // array.push()

              this._dataDirections.push({ 
                      direccion: place.description, 
                      placeid: place.placeId, 
                      observacion: "", 
                      lat: 0, 
                      long: 0, 
                      es_principal: 'S', 
                      alias: "",
                      selected: false,
                    });
              })
              
           }, (error => {              
              console.dir(error)
          }));

  }

    public onSubmit(args) {
        let textfield = <TextField>args.object;
        let value = textfield.text;
        let size = value.length;

        if (this.isDirectionSelected && isAndroid) {
          textfield.android.clearFocus();
        }
        
        // si la dire fue seleccionada, si es nulo, menor a 4 caracteres y/o es impar evito busqueda
        if ( this.isDirectionSelected || (!value) || (size <= 6) || (size%2 > 0 )) { return };
        
        this._dataDirections = new ObservableArray<any>();

        this.googlePlacesAutoComplete.search(textfield.text, 'ar', 'geocode').then((placeResult: any) => {
   
           if (!placeResult) { return; }
              
              console.dir(placeResult.length);
              
              placeResult.forEach((place)=>{
                  console.dir(place);

                  // array.push()

              this._dataDirections.push({ 
                      direccion: place.description, 
                      placeid: place.placeId, 
                      observacion: "", 
                      lat: 0, 
                      long: 0, 
                      es_principal: 'S', 
                      alias: "",
                      selected: false,
                    });
              })
              
           }, (error => {              
              console.dir(error)
          }));

  }
  
  public selectDirection(direction) {
      console.dir("@selectDirection");
      
      this.textField.nativeElement;
      this.textField.nativeElement.text = direction.direccion;
      this.isDirectionSelected = true;

      this._dataDirections.map(a=>a.selected=false);
      
      this.googlePlacesAutoComplete.getPlaceById(direction.placeid).then((place)=>{
        // console.dir(place);
        console.dir(place.data.result.address_components);
        
        direction.selected = true;
        direction.lat = place.latitude;
        direction.long = place.longitude;          
        
        //set de datos
        place.data.result.address_components.forEach((adress_component)=>{

                if(adress_component.types.includes('street_number')){
                    direction.number = adress_component.short_name;
                }

                if(adress_component.types.includes('route')){
                    direction.street = adress_component.short_name;    
                }

                if(adress_component.types.includes('locality')){
                    direction.city = adress_component.short_name;    
                }else if(adress_component.types.includes('sublocality')){
                    direction.city = adress_component.short_name;    
                }

                if(adress_component.types.includes('administrative_area_level_1')){
                    direction.state = adress_component.short_name;    
                }

                if(adress_component.types.includes('administrative_area_level_2')){
                    direction.state_2 = adress_component.short_name;    
                }

                if(adress_component.types.includes('country')){
                    direction.country = adress_component.short_name;    
                }

                if(adress_component.types.includes('floor')){
                    direction.flat_apartment = adress_component.short_name;    
                }

                if(adress_component.types.includes('postal_code')){
                    direction.postal_code = adress_component.short_name;    
                }

        })

         
        direction.number = direction.number ? direction.number : 0;
        direction.street = direction.street ? direction.street : '';
        direction.city = direction.city ? direction.city : direction.state ? direction.state : '' ;
        direction.state = direction.state ? direction.state : direction.state_2 ? direction.state_2 : '' ;
        direction.country = direction.country ? direction.country : 'Argentina' ;
        direction.flat_apartment = direction.flat_apartment ? direction.flat_apartment : '' ;
        direction.postal_code = direction.postal_code ? direction.postal_code : '' ;
        
        direction.direccion = direction.street +' '+direction.number+', '+direction.city+', '+direction.state;

        this._direction = direction;
        this.isDirectionSelected = true;
        this.textField.nativeElement.dismissSoftInput();

      })
    }

    backToPage(){
     this.backPage.emit();
    }

    nextToPage(){
      if (this.isDirectionSelected)
      this.meetup.address = this._direction;
      this.nextPage.emit(1);
    }

    onFocus() {
        const textField = this.textField.nativeElement;

        // animate the label sliding up and less transparent.
        textField.animate({
            translate: { x: 0, y: - 25 },
            opacity: 1,
        }).then(() => { }, () => { });

        // set the border bottom color to green to indicate focus
        textField.borderBottomColor = new Color('#4b6ca5');
    }

    onBlur() {
        const textField = this.textField.nativeElement;

        // if there is text in our input then don't move the label back to its initial position.
        if (!textField.text) {
            textField.animate({
                translate: { x: 0, y: 0 },
                opacity: 0.4
            }).then(() => { }, () => { });
        }
        // reset border bottom color.
        textField.borderBottomColor = new Color('#4b6ca5');
    }
}
