import { Component, ElementRef, Input, Output, EventEmitter, ViewChild } from "@angular/core";
import * as frame from "tns-core-modules/ui/frame";
import { isIOS } from "tns-core-modules/platform";
import { Color } from "tns-core-modules/color";

@Component({
    selector: "paso-cuatro-component",
    templateUrl: 'paso-cuatro.component.html',
    moduleId: module.id,
    styles: [``]
})
export class PasoCuatroComponent {


    @Input() meetup: any;
    @Output() submitMeetup = new EventEmitter();
    @Output() resetSearch = new EventEmitter();

    constructor() {

    }

    resetPage() {
        this.resetSearch.emit();
    }

    newMeetup() {
        this.submitMeetup.emit();
    }
}    
