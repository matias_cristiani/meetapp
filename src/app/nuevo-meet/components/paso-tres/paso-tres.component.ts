import { Component, ElementRef, Input, Output, EventEmitter, ViewChild } from "@angular/core";
import * as frame from "tns-core-modules/ui/frame";
import { isIOS, isAndroid, device } from "tns-core-modules/platform";
import { SearchBar } from "tns-core-modules/ui/search-bar";
import * as appSettings from "tns-core-modules/application-settings";
import { ObservableArray } from "tns-core-modules/data/observable-array";
import { ApiService } from "../../../shared/services/api.service";

declare var NSMutableArray, NSIndexPath;

@Component({
    selector: "paso-tres-component",
    templateUrl: 'paso-tres.component.html',
    moduleId: module.id
})
export class PasoTresComponent {    
    
    @Input() dataUsersFilter: any;
    @Input() dataUsers: any;
    @Input() meetup: any;
    
    @Output() nextPage = new EventEmitter();
    @Output() backPage = new EventEmitter();
    
    public selected: boolean;
    public isLoading: boolean;

    constructor(public _apiService: ApiService) {
        this.selected = false;
        this.isLoading = false;
    }

    ngAfterViewInit(){
      //review meetup participations
    }

    public sBLoaded(args) {
        console.dir("@sBLoaded");
        let searchbar=<SearchBar>args.object;

        if(isAndroid){ searchbar.android.clearFocus() };
    }
    
    public onClear(args){
        this.filterSubCategorias('');
    }

    public onSubmit(args){
        let searchBar = <SearchBar>args.object;
        let value = searchBar.text;

        this.filterSubCategorias(value);
    }

   public filterSubCategorias(value) {
        console.dir('@filterSubCategorias-dos');
        let valueSearch = String(value).toLowerCase();
        let result = [];
        this.isLoading = true;

        setTimeout(()=>{
            this.dataUsersFilter.forEach((item)=>{
                if ( (item.name.toLowerCase()).indexOf(valueSearch) > -1 ) {
                    result.push(item);
                }
            })
            
            if (result && result.length > 0 ) {
              this.dataUsers = [];
              
              result.forEach((r)=>{
                r.selected = false;
                this.dataUsers.push(r);
              })  
            }

            this.isLoading = false;

        },600)
    }

    onItemTap(dataItem) {
          
          if (dataItem.selected) {
            dataItem.selected = false;
            return;
          }
          
          dataItem.selected = true;

    }

    backToPage() { 
        this.backPage.emit(1);
    }
    
    navigateToConfirm() {

        this.dataUsers.forEach((user) =>{
          if (user.selected) {
            this.meetup.participations_ids.push(user)
          }
        })

        if (this.meetup.participations_ids.length == 0) {
          alert("Debe seleccionar al menos un invitado")
          return;
        }

        this.nextPage.emit(1);
    }

}
