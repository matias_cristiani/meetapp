import { Component, ElementRef, Input, Output, EventEmitter, ViewChild } from "@angular/core";
import * as frame from "tns-core-modules/ui/frame";
import { Feedback, FeedbackType, FeedbackPosition } from "nativescript-feedback";
import { isIOS, isAndroid } from "tns-core-modules/platform";
import { Color } from "tns-core-modules/color";
import { TextField } from "tns-core-modules/ui/text-field";
import { ObservableArray } from "tns-core-modules/data/observable-array";
import { GooglePlacesAutocomplete } from 'nativescript-google-places-autocomplete';

import { DateTimePicker } from "nativescript-datetimepicker";
import { EventData } from "tns-core-modules/data/observable";
import { Button } from "tns-core-modules/ui/button";

@Component({
    selector: "paso-one-component",
    templateUrl: 'paso-one.component.html',
    moduleId: module.id,
    styles: [``],
    
})

export class PasoOneComponent {
    
    public dateTime;
    public dateNow: Date;
    public dateSelected: boolean;
    private feedback: Feedback;

    @Input() meetup: any;
    @Output() nextPage = new EventEmitter();
    @Output() backHome = new EventEmitter();

    constructor(
    ) {

      this.feedback = new Feedback()
      this.dateSelected = false;
    }

    onPickDateTimeTap(args: EventData): void {
        
        this.dateNow = (this.meetup.date) ? new Date(this.meetup.date) : new Date();

        DateTimePicker.pickDate({
            context: (<Button>args.object)._context,
            date: this.dateNow,
            title: "Seleccionar día",
            locale: "es_ES",
        }).then((selectedDate: Date) => {
            if (selectedDate) {
                DateTimePicker.pickTime({
                    context: (<Button>args.object)._context,
                    time: selectedDate,
                    title: "Elegi una hora",
                    locale: "es_ES",
                }).then((selectedDateTime: Date) => {
                    if (selectedDateTime) {
                        this.dateSelected = true;
                        this.dateTime = this.getFormattedDate(selectedDateTime) + ' ' + this.getFormattedTime(selectedDateTime);
                        this.meetup.date = this.dateTime;
                    }
                });
            }
        });
    }

    getFormattedDate(date: Date): string {
        const d = date.getDate();
        const m = date.getMonth() + 1;
        const y = date.getFullYear();
        return (d < 10 ? '0' : '') + d + '.' + (m < 10 ? '0' : '') + m + '.' + y;
    }

    getFormattedTime(date: Date): string {
        const h = date.getHours();
        const m = date.getMinutes();
        return (h < 10 ? '0' : '') + h + ':' + (m < 10 ? '0' : '') + m;
    }


    // navigations

    backToHome(){
     this.backHome.emit();
    }

    nextToPage(){

      if (this.meetup.title.length == 0 || this.meetup.subject.length == 0  || !this.dateSelected) {
          return;
      }

      this.nextPage.emit(1);
    }

}
