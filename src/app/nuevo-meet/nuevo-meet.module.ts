import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptDateTimePickerModule } from "nativescript-datetimepicker/angular";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";

import { NuevoMeetRoutingModule } from "./nuevo-meet-routing.module";
import { NuevoMeetComponent } from "./nuevo-meet.component";

import { SharedModule } from "../shared/shared.module"
import { PagerModule } from "nativescript-pager/angular";

import { PasoOneComponent } from "./components/paso-one/paso-one.component";
import { PasoDosComponent } from "./components/paso-dos/paso-dos.component";
import { PasoTresComponent } from "./components/paso-tres/paso-tres.component";
import { PasoCuatroComponent } from "./components/paso-cuatro/paso-cuatro.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        NuevoMeetRoutingModule,
        NativeScriptDateTimePickerModule,
        NativeScriptUIListViewModule,
        NativeScriptFormsModule,
        PagerModule,
        SharedModule
    ],
    declarations: [
        NuevoMeetComponent,
        PasoOneComponent,
        PasoDosComponent,
        PasoTresComponent,
        PasoCuatroComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class NuevoMeetModule { }
