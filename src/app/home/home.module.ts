import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptUICalendarModule } from "nativescript-ui-calendar/angular";

import { HomeRoutingModule } from "./home-routing.module";
import { HomeComponent } from "./home.component";

import { SharedModule } from "../shared/shared.module"
import { ClimaComponent } from "./components/clima/clima.component"
import { CalendarComponent } from "./components/calendar/calendar.component"
import { MeetupComponent } from "./components/meetup/meetup.component"

import { FahrenheitPipe } from "../shared/pipes/fahrenheit.pipe"
import { MomentPipe } from "../shared/pipes/moment.pipe"

@NgModule({
    imports: [
        NativeScriptUICalendarModule,
        NativeScriptCommonModule,
        HomeRoutingModule,
        SharedModule
    ],
    declarations: [
        CalendarComponent,
        MeetupComponent,
        ClimaComponent,
        FahrenheitPipe,
        HomeComponent,
        MomentPipe
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class HomeModule { }
