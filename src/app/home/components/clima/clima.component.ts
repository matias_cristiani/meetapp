import { Component, ElementRef, Input, Output, EventEmitter, ViewChild } from "@angular/core";
import * as frame from "tns-core-modules/ui/frame";

@Component({
    selector: "clima-component",
    templateUrl: 'clima.component.html',
    moduleId: module.id,
    styles: [``],
    
})
export class ClimaComponent {
   
    @Input() weather: any;
    @Output() emitUpdatePress = new EventEmitter();


    constructor() {
    }

    onPress(args) {
    	this.emitUpdatePress.emit(args)
    }


}
