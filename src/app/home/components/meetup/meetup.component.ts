import { Component, ElementRef, Input, Output, EventEmitter, ViewChild } from "@angular/core";
import * as appSettings from "tns-core-modules/application-settings";
import * as frame from "tns-core-modules/ui/frame";

@Component({
    selector: "meetup-component",
    templateUrl: 'meetup.component.html',
    moduleId: module.id,
    styles: [``],
    
})
export class MeetupComponent {
    
    @Input() meetup: any;
    @Input() user: any;
    @Input() temperature: any;
    @Input() isowner: boolean;

    @Output() emitOnPress = new EventEmitter();

    public isCheckin: boolean;
    public count: number;
    constructor() {
        this.isCheckin = false;
    }

    getLabel(){
        // isOwner
        
        this.count = this.meetup.participations_ids.length;
        if (this.isowner) {
            let cajones = this.getBoxBeers(this.meetup.participations_ids.length);
            return (cajones==1) ? ('COMPRA '+cajones+ ' BOX DE CERVEZA') : ('COMPRA '+cajones+ ' BOXS DE CERVEZAS');
        }


        this.meetup.participations_ids.forEach((p)=>{
            let key = (p && p.id) ? p.id : p;
            console.dir(key,this.user.id);    
            if(key == this.user.id || key.indexOf(this.user.id)>-1){
                this.isCheckin= true;
                return 'TE ESPERAMOS'
            }
        })

        return 'PARTICIPAR';
    }
    
    onPress() {
         
        this.meetup.participations_ids.push(('users/'+this.user.id));
        
        console.dir(this.meetup);
        
        this.emitOnPress.emit(this.meetup);

    }

    getBoxBeers(participations) {
        // 1 participante
        let cajon = 6;
        let cerveza = 0;
        let result = 0;
        // 1 caja = 6 
        // n participations
        // 20 - 24 = 1 
        // - 20 = 0.75
        // > 24 = 2

        if (this.temperature > 24) {
            // mayor a 24
            // 2 cervezas
            cerveza = cerveza + 2;

        }else if (this.temperature > 19){
            // entre 24 y 19
            cerveza = cerveza + 1;
        }else{
            // menos de 20
            cerveza = cerveza + 0.75;
        }
        
        //lo trasladamos a cada participante
        cerveza = cerveza * participations;
        
        //exactly
        result = cerveza / cajon;

        //para que sobre
        result =  Math.ceil(result)
        return result;
    }

    getStatus() {
        
    }

}