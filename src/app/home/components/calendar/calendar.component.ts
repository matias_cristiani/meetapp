import { Component, ElementRef, Input, Output, EventEmitter, ViewChild } from "@angular/core";
import * as frame from "tns-core-modules/ui/frame";

@Component({
    selector: "calendar-component",
    templateUrl: 'calendar.component.html',
    moduleId: module.id,
    styles: [``],
    
})
export class CalendarComponent {
   
    @Input() calendar: any;
    @Output() emitUpdatePress = new EventEmitter();


    constructor() {

    }

   
}
