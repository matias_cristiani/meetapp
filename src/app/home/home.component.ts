import { Component, ElementRef, NgZone, OnInit, AfterViewInit, OnDestroy, ViewChild } from "@angular/core";
import { InternetConnectionService } from "../shared/services/internet-connection.service";
import { Feedback, FeedbackType, FeedbackPosition } from "nativescript-feedback";
import { GeolocationService } from "../shared/services/geolocation-service";
import { ObservableArray } from "tns-core-modules/data/observable-array";
import * as appSettings from "tns-core-modules/application-settings";
import { RouterExtensions } from "nativescript-angular/router";
import { ApiService } from "../shared/services/api.service";
import * as firebase from "nativescript-plugin-firebase";
import * as appversion from "nativescript-appversion";
import { Page } from "tns-core-modules/ui/page";
import { Color } from "tns-core-modules/color";

@Component({
    selector: "Home",
    moduleId: module.id,
    templateUrl: "./home.component.html",
    styleUrls: ["./home.component.css"],
    animations:[]
})

export class HomeComponent implements OnInit, AfterViewInit, OnDestroy{
    geolocation$;
    connection$;
    user$;
    meetups$;
    
    public dataUser: any;
    public dataWeather: any;
    
    public dataMeMeetups: Array<any>;    
    public dataOtherMeetups: Array<any>;    

    public isLoading: boolean;
    public connectionStatus: boolean;
    private feedback: Feedback;

    
    constructor(
        private _apiService: ApiService,
        private _geolocation: GeolocationService,
        private _internetConnection: InternetConnectionService,
        private _routerExtensions: RouterExtensions,
        private _ngZone: NgZone,
        private page: Page) {
        
        this.dataMeMeetups = new Array();
        this.dataOtherMeetups = new Array();

        this.dataWeather = { 
            temperature: 0,
            highTemperature: 0,
            lowTemperature: 0,
            description: "",
            city: "",
            iconLink: 'assets/home/sun-disabled.png',
            utcTime: new Date()
        }
        
        this.page.actionBarHidden = false;
        this.page.backgroundSpanUnderStatusBar = false;
        this.page.className = "page-home-container";
        this.page.statusBarStyle = "dark";
        this.feedback = new Feedback()
    }

     ngOnInit(): void {
        this.isLoading = true;

        this.connection$ = this._internetConnection.connectionStatus$.subscribe(data => {
            this.connectionStatus = data.valueOf();
        });

        this.geolocation$ = this._geolocation.geolocationPosition$.subscribe(data => {
            console.dir(data);
            if (data.latitude == 0) {
                return;
            }

            this.initializeWeatherComponent(data);
        });
        
        this.user$ = this._apiService.User$.subscribe(value => {

            this.dataUser = value;

            this._apiService.firestoreGet('roles', this.dataUser.rol.id).then((res)=>{

                if (res.status != "success") {
                    this.showWarning("No hemos podido obtener los datos del clima!");
                    return;
                }

                this.dataUser.rol_str = res.data.label;
                this.dataUser.isAdmin = (res.data.label=="Administrador") ? true : false;;
            })
            
        })

        this.meetups$ = this._apiService.Meetup$.subscribe(values=>{
            this.dataMeMeetups = new Array();
            this.dataOtherMeetups = new Array();
            
            values.forEach((val)=>{

                if( val.owner_id.id == appSettings.getString('user_id') ){
                    this.dataMeMeetups.push(val);
                }else{
                    this.dataOtherMeetups.push(val);
                }
            })
        })

        // this._apiService.apiForGetMeetups();
        
    }

    ngOnDestroy(): void {
        if (this.connection$){
            this.connection$.unsubscribe();
        }
    
        if (this.user$) {
            this.user$.unsubscribe();
        }
    }

    ngAfterViewInit(){
        this._geolocation.getCurrentLocation().then((data: any)=> {
            this.initializeWeatherComponent(data);
        })
    }

    initializeWeatherComponent(data){

            this._apiService.apiForGetWeather({ longitude: data.longitude, latitude: data.latitude}).then((res: any)=>{
                
                if (res.status != "success") {
                    this.showWarning("No hemos podido obtener los datos del clima!");
                    return;
                }

                // console.dir(res.data.observation[0]);

                this.dataWeather.highTemperature =  Math.round(res.data.observation[0].highTemperature);
                this.dataWeather.lowTemperature =  Math.round(res.data.observation[0].lowTemperature);
                this.dataWeather.temperature =  Math.round(res.data.observation[0].temperature);

                this.dataWeather.city = res.data.observation[0].city;
                this.dataWeather.description = res.data.observation[0].description;

                this.dataWeather.utcTime =  res.data.observation[0].utcTime;
                this.dataWeather.iconLink =  res.data.observation[0].iconLink;
                

                this.isLoading = false;
        })
    }
    
    onLogOut() {
     
        //clear subscribed
        const firebase = require("nativescript-plugin-firebase");
        firebase.unsubscribeFromTopic("user_"+this.dataUser.id).then(() => {
                console.log("deSubscribed to user_"+this.dataUser.id)
        });
        
        //clear data
        appSettings.clear();
         
        this._apiService.setUser({
          id: 0,
          doc_number: 0,
          name: "",
          last_name: "",
          status: '0',
          avatar: null,
          address: null,
          address_str: null,
          rol: "",
          rol_str: "",
          isAdmin: false,
          phone_number: "",
          authorization: "",
          notification_token: "",
          email: "",
          password: ""
        })

        this.navigateTo('login');

    }

    /* actions */
    meetPress(meet){
        if(this.isLoading || !this.connectionStatus) return;

    }
    
    
    checkInMeetup(args){
        
        this._apiService.firestoreSet('meetups', args.id, args, true).then((res: any)=>{

              if (res.status != 'success'){
                  this.isLoading = false;
                  this.showWarning(res.message);
                  return;
              }

              this.isLoading = false;
             
              this.showSuccess("Meetup actualizado correctamente.");
            
        })
    }

    newMeetup(args){
        if(this.isLoading || !this.connectionStatus) return;
        
        this._routerExtensions.navigate(['nuevo-meetup',0], {
          clearHistory: true,
          animated: true,
          transition: {
            name: "slideRight",
            duration: 200,
            curve: "ease"
          }
        });
    }

    /* feedbacks */
    
    showWarning(message){
        this.feedback.warning({
              message: message,
              position: FeedbackPosition.Top,
              type: FeedbackType.Warning,
              backgroundColor: new Color("#df4a32"),
        });
    }

    showSuccess(message){
        this.feedback.warning({
              message: message,
              position: FeedbackPosition.Top,
              type: FeedbackType.Success,
              backgroundColor: new Color("#95ba65"),
        });
    }

    /* navigations */
    
    public navigateTo(page, params = null) {
      this._ngZone.run(() => {
        
        let toPage = (params) ? [page].concat(params) : [page];
        console.dir(toPage);
        this._routerExtensions.navigate(toPage, {
          clearHistory: true,
          animated: true,
          transition: {
            name: "slideRight",
            duration: 200,
            curve: "ease"
          }
        });

      });
   }
}
