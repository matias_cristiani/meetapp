import { InternetConnectionService } from "../shared/services/internet-connection.service";
import { trigger, state, transition, style, animate } from '@angular/animations';
import { Feedback, FeedbackType, FeedbackPosition } from "nativescript-feedback";
import * as appSettings from "tns-core-modules/application-settings";
import { Component, NgZone, OnInit, ViewChild} from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { ApiService } from "../shared/services/api.service";
import { Page } from "tns-core-modules/ui/page";
import { Color } from "tns-core-modules/color";

@Component({
    selector: "Profile",
    moduleId: module.id,
    templateUrl: "./profile.component.html",
    styleUrls: ["./profile.component.css"],
    animations: []
})

export class ProfileComponent implements OnInit{
    connection$;
    user$;
    
    @ViewChild("absoluteLayout",{ static: false }) absoluteLayout: any;
    
    private feedback: Feedback;
    
    public dataUser: any;
    public connectionStatus: boolean;
    public isLoading: boolean;

    renderView = false;
    renderViewTimeout: any;
    
    constructor(
        private _internetConnection: InternetConnectionService,
        private _routerExtensions: RouterExtensions,
        private _apiService: ApiService,
        private _ngZone: NgZone,
        private page: Page) {

        this.feedback = new Feedback()
        this.isLoading = false;

    }
    
    ngOnInit() {
        this.isLoading = true;

        this.connection$ = this._internetConnection.connectionStatus$.subscribe(data => {
            this.connectionStatus = data.valueOf();
        });
        
        this.user$ = this._apiService.User$.subscribe(value => {
            
            this.dataUser = value;

            this._apiService.firestoreGet('roles', this.dataUser.rol.id).then((res)=>{

                if (res.status != "success") {
                    return;
                }

                this.dataUser.rol_str = res.data.label;
                this.dataUser.isAdmin = (res.data.label=="Administrador") ? true : false;;
            })


            if (this.dataUser.address && this.dataUser.address.id) {
              
             this._apiService.firestoreGet('addresses', this.dataUser.address.id).then((res)=>{

                if (res.status != "success") {
                    return;
                }
                
                console.dir(res.data);

                this.dataUser.address_str = res.data.direccion;
                this.dataUser.isAdmin = (res.data.label=="Administrador") ? true : false;;
            })
            
            }else{
              console.dir(this.dataUser.address);
              this.dataUser.address = { id: 0};
            }
        })
    }
  
    ngAfterContentInit() {
        this.renderViewTimeout = setTimeout(() => {
            this.renderView = true;
            this.isLoading = false; 
      }, 1200);
    }

    ngOnDestroy(): void {
        if (this.connection$){
            this.connection$.unsubscribe();
        }
    
        if (this.user$) {
            this.user$.unsubscribe();
        }
        clearTimeout(this.renderViewTimeout);        
    }

    public updateProfile() {
        this.isLoading = true;
        
        console.dir(this.dataUser);
        
        this._apiService.firestoreSet('users', this.dataUser.id, this.dataUser, true).then((res: any)=>{

              if (res.status != 'success'){
                  this.isLoading = false;
                  this.showWarning(res.message);
                  return;
              }

              this.isLoading = false;
             
              this.showSuccess("Usuario actualizado correctamente.");
            
              setTimeout(()=>{
                this.navigateTo('profile')
              }, 1000)
        })

    }
    
    /* feedbacks */
    
    private showWarning(message){
        this.feedback.warning({
              message: message,
              position: FeedbackPosition.Top,
              type: FeedbackType.Warning,
              backgroundColor: new Color("#df4a32"),
        });
    }


    private showSuccess(message){
        this.feedback.warning({
              message: message,
              position: FeedbackPosition.Top,
              type: FeedbackType.Success,
              backgroundColor: new Color("#95ba65"),
        });
    }
    

    /* navigations */
    
    public navigateTo(page, params = null) {
      this._ngZone.run(() => {
        
        let toPage = (params) ? [page].concat(params) : [page];
        console.dir(toPage);
        this._routerExtensions.navigate(toPage, {
          clearHistory: true,
          animated: true,
          transition: {
            name: "slideLeft",
            duration: 200,
            curve: "ease"
          }
        });

      });
   }
}
