import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";
import { SetDireccionRoutingModule } from "./set-direccion-routing.module";
import { SetDireccionComponent } from "./set-direccion.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        SetDireccionRoutingModule, 
        NativeScriptUIListViewModule,
        NativeScriptFormsModule,
    ],
    declarations: [
        SetDireccionComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class SetDireccionModule { }
