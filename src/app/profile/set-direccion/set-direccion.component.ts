import { Component, ElementRef, NgZone, OnInit, OnDestroy, ViewChild } from "@angular/core";
import { InternetConnectionService } from "../../shared/services/internet-connection.service";
import { trigger, state, transition, style, animate } from '@angular/animations';
import { Feedback, FeedbackType, FeedbackPosition } from "nativescript-feedback";
import { GooglePlacesAutocomplete } from 'nativescript-google-places-autocomplete';
import { ObservableArray } from "tns-core-modules/data/observable-array";
import { RouterExtensions } from "nativescript-angular/router";
import { isIOS, isAndroid } from "tns-core-modules/platform";
import { ApiService } from "../../shared/services/api.service";
import { SearchBar } from "tns-core-modules/ui/search-bar";
import * as firebase from "nativescript-plugin-firebase";
import * as app from "tns-core-modules/application";
import { ActivatedRoute } from "@angular/router";
import { Page } from "tns-core-modules/ui/page";
import { Color } from "tns-core-modules/color";
import * as appSettings from "tns-core-modules/application-settings";

declare var android;

@Component({
    selector: "SetDireccion",
    moduleId: module.id,
    templateUrl: "./set-direccion.component.html",
    styleUrls: ["./set-direccion.component.css"],
    animations: [
    
        trigger('slide-in-out', [
          transition(':enter', [
            style({transform: 'translateX(-100%)'}),
            animate('1000ms ease-in', style({transform: 'translateX(0%)'}))
          ]),
          transition(':leave', [
            animate('1000ms ease-in', style({transform: 'translateX(-100%)'}))
          ])
        ])

    ]
})

export class SetDireccionComponent implements OnInit, OnDestroy {
    user$;
    config$;
    connection$;
 
    @ViewChild("searchBar", { static: false }) searchBar: ElementRef;

    public isLoading: boolean;
    public connectionStatus: boolean;
    public isDirectionSelected: boolean;

    private dataUser: any;
    private dataConfig: any;    
    public selectedStore: any;
    public _direction: any;

    private _dataDirections: ObservableArray<any>;
    private feedback: Feedback;
    private googlePlacesAutoComplete: GooglePlacesAutocomplete;
    private userid: any;
    private addressid: any;

    constructor(
        private _apiService: ApiService,
        private _internetConnection: InternetConnectionService,
        private _routerExtensions: RouterExtensions,
        private _routeActive: ActivatedRoute,
        private _ngZone: NgZone,
        private page: Page) {

        this.feedback = new Feedback()
        this.googlePlacesAutoComplete = new GooglePlacesAutocomplete("AIzaSyB28j7uM5ZN0hbeBev-BNLna6g1HGaZB8I");
        
        this._direction = { 
                    direccion: "", 
                    placeid: "", 
                    observacion: "", 
                    selected: false,
                    lat: 0, 
                    long: 0, 
                  };
        
        this.addressid = 0;
        this.userid = 0;
        
        this.connectionStatus = true;
        this._dataDirections = new ObservableArray<any>();
        this.isDirectionSelected = false;
        this.isLoading = false;
        
    }

   
     ngOnInit(): void {
        this.connection$ = this._internetConnection.connectionStatus$.subscribe(data => {
            this.connectionStatus = data.valueOf();
        });
          
        this.config$ = this._apiService.Config$.subscribe(value => {
            this.dataConfig = value;
        })

         this.user$ = this._apiService.User$.subscribe(value => {
            
            this.dataUser = value;

            this._apiService.firestoreGet('roles', this.dataUser.rol.id).then((res)=>{

                if (res.status != "success") {
                    return;
                }

                this.dataUser.rol_str = res.data.label;
                this.dataUser.isAdmin = (res.data.label=="Administrador") ? true : false;;
            })


             this._apiService.firestoreGet('addresses', this.dataUser.addresses.id).then((res)=>{

                if (res.status != "success") {
                    return;
                }
                
                this._direction = res.data;
                this._direction.direccion = "";

                this.dataUser.address_str = res.data.direccion;
                this.dataUser.isAdmin = (res.data.label=="Administrador") ? true : false;;
            })
            
        })
        this._routeActive.params.subscribe((params) => {
            this.addressid = params["addressid"]
            this.userid = params["userid"]
        });
        
    }

    ngOnDestroy(): void {
        if (this.connection$){
            this.connection$.unsubscribe();
        }
        if (this.config$){
            this.config$.unsubscribe();
        }
        if (this.user$){
            this.user$.unsubscribe();
        }
    }
   
    get dataDirections(): ObservableArray<any> {
        return this._dataDirections;
    }
    
    public sBLoaded(args) {
        console.dir("@sBLoaded");
        let searchbar=<SearchBar>args.object;
        
        if(isAndroid){
            searchbar.android.clearFocus();
        }

    }

    public selectDirection(direction) {
      console.dir("@selectDirection");
      
      this._dataDirections.map(a=>a.selected=false);
      
      this.googlePlacesAutoComplete.getPlaceById(direction.placeid).then((place)=>{
        // console.dir(place);
        console.dir(place.data.result.address_components);
        
        direction.selected = true;
        direction.lat = place.latitude;
        direction.long = place.longitude;          
        
        //set de datos
        place.data.result.address_components.forEach((adress_component)=>{

                if(adress_component.types.includes('street_number')){
                    console.log("YES");
                    direction.number = adress_component.short_name;
                }

                if(adress_component.types.includes('route')){
                    direction.street = adress_component.short_name;    
                }

                if(adress_component.types.includes('locality')){
                    direction.city = adress_component.short_name;    
                }else if(adress_component.types.includes('sublocality')){
                    direction.city = adress_component.short_name;    
                }

                if(adress_component.types.includes('administrative_area_level_1')){
                    direction.state = adress_component.short_name;    
                }

                if(adress_component.types.includes('administrative_area_level_2')){
                    direction.state_2 = adress_component.short_name;    
                }

                if(adress_component.types.includes('country')){
                    direction.country = adress_component.short_name;    
                }

                if(adress_component.types.includes('floor')){
                    direction.flat_apartment = adress_component.short_name;    
                }

                if(adress_component.types.includes('postal_code')){
                    direction.postal_code = adress_component.short_name;    
                }

        })

        
        direction.number = direction.number ? direction.number : 0;
        direction.street = direction.street ? direction.street : '';
        direction.city = direction.city ? direction.city : direction.state ? direction.state : '' ;
        direction.state = direction.state ? direction.state : direction.state_2 ? direction.state_2 : '' ;
        direction.country = direction.country ? direction.country : 'Argentina' ;
        direction.flat_apartment = direction.flat_apartment ? direction.flat_apartment : '' ;
        direction.postal_code = direction.postal_code ? direction.postal_code : '' ;
        
        direction.direccion = direction.street +' '+direction.number+', '+direction.city+', '+direction.state;

        this._direction = direction;
        this.isDirectionSelected = true;

        try{
          if (isAndroid) {
            this.searchBar.nativeElement.android.clearFocus();
          }else{
            this.searchBar.nativeElement.dismissSoftInput();
          }
        }catch(e){
          
        }

      })
    }


    public onClear(args) {
        console.dir("@onClear");
        this._direction.direccion = "";
        this.isDirectionSelected = false;
        this._dataDirections = new ObservableArray<any>();
    }

    public onTextChanged(args) {
        let searchBar = <SearchBar>args.object;
        let value = searchBar.text;
        let size = value.length;
        let reading = true;
        // si la dire fue seleccionada, si es nulo, menor a 4 caracteres y/o es impar evito busqueda
        if ( this.isDirectionSelected || (!value) || (size <= 6) || (size%2 > 0 )) { return };
       
        this.googlePlacesAutoComplete.search(searchBar.text,'ar', 'geocode').then((placeResult: any) => {
           let array = [];   
           if (!placeResult) { return; }
            
              placeResult.forEach((place)=>{
                  console.dir(place);

                  array.push({ 
                      direccion: place.description, 
                      placeid: place.placeId, 
                      observacion: "", 
                      lat: 0, 
                      long: 0, 
                      es_principal: 'S', 
                      alias: "",
                      selected: false
                    });
              })

              
              this._dataDirections = new ObservableArray<any>(array);
              

              
           }, (error => {              
              console.dir(error)
          }));
           
    }

    public onSubmit(args) {
        let searchBar = <SearchBar>args.object;
        let value = searchBar.text;
        let size = value.length;

        if (this.isDirectionSelected && isAndroid) {
          searchBar.android.clearFocus();
        }
        
        // si la dire fue seleccionada, si es nulo, menor a 4 caracteres y/o es impar evito busqueda
        if ( this.isDirectionSelected || (!value) || (size <= 6) || (size%2 > 0 )) { return };

        
        if(isAndroid){
          searchBar.android.clearFocus();
        }

        this.googlePlacesAutoComplete.search(searchBar.text, 'ar', 'geocode').then((placeResult: any) => {
           let array = [];   
           if (!placeResult) { return; }
              
              console.dir(placeResult.length);
              
              placeResult.forEach((place)=>{
                  console.dir(place);

                  array.push({ 
                      direccion: place.description, 
                      placeid: place.placeId, 
                      observacion: "", 
                      lat: 0, 
                      long: 0, 
                      es_principal: 'S', 
                      alias: "",
                      selected: false,
                    });
              })

              console.dir(array.length);

              this._dataDirections = new ObservableArray<any>(array);
              
           }, (error => {              
              console.dir(error)
          }));

    }

    /* show's */
    showWarning(message){
        this.feedback.warning({
              message: message,
              position: FeedbackPosition.Top,
              type: FeedbackType.Warning,
              backgroundColor: new Color("#df4a32"),
        });
    }

    showSuccess(message){
        this.feedback.warning({
              message: message,
              position: FeedbackPosition.Top,
              type: FeedbackType.Success,
              backgroundColor: new Color("#95ba65"),
        });
    }

    updateDirection() {
        this.isLoading  = true;
        
        console.dir(this.addressid);        

        if (this.addressid != 0) {
          this.newDirection();
          return;
        }

        this._apiService.firestoreSet('addresses', this.addressid, this._direction, true).then((res: any)=>{

              if (res.status != 'success'){
                  this.isLoading = false;
                  this.showWarning(res.message);
                  return;
              }

              this.isLoading = false;
             
              this.showSuccess("Direccion cargada correctamente.");
            
              setTimeout(()=>{
                this.navigateTo('profile')
              }, 1000)
          })

    

    }

    newDirection(){
      this._apiService.firestoreAdd('addresses', this._direction).then((res: any)=>{

              if (res.status != 'success'){
                  this.isLoading = false;
                  this.showWarning(res.message);
                  return;
              }

              this.isLoading = false;
             
              this.showSuccess("Direccion cargada correctamente.");

              this._apiService.firestoreSet('users', this.userid, {
                address: res.data
              }, true).then((res: any)=>{
            
              setTimeout(()=>{
                this.navigateTo('profile')
              }, 1000)

              })
          })
    }

    /* navigations */
    
    public navigateTo(page, params = null) {
      this._ngZone.run(() => {
        
        let toPage = (params) ? [page, params] : [page];

        this._routerExtensions.navigate(toPage, {
          clearHistory: true,
          animated: true,
          transition: {
            name: "slideLeft",
            duration: 200,
            curve: "ease"
          }
        });

      });
   }
}
