import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { SetDireccionComponent } from "./set-direccion.component";

const routes: Routes = [
    { path: "", component: SetDireccionComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class SetDireccionRoutingModule { }
