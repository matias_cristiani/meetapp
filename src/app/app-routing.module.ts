import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { LoggedInLazyLoadGuard } from "./logged-in-lazy-load.guard";

const routes: Routes = [
    { path: "", redirectTo: "/login", pathMatch: "full" },
    { path: "login", loadChildren: () => import("~/app/login/login.module").then(m => m.LoginModule) },
    { path: "register", loadChildren: () => import("~/app/register/register.module").then(m => m.RegisterModule) },
    { path: "profile", loadChildren: () => import("~/app/profile/profile.module").then(m => m.ProfileModule) , canLoad: [LoggedInLazyLoadGuard] },
    { path: "set-direccion/:addressid/:userid", loadChildren: () => import("~/app/profile/set-direccion/set-direccion.module").then(m => m.SetDireccionModule) , canLoad: [LoggedInLazyLoadGuard] },
    { path: "home", loadChildren: () => import("~/app/home/home.module").then(m => m.HomeModule) , canLoad: [LoggedInLazyLoadGuard] },
    { path: "notificaciones", loadChildren: () => import("~/app/notificaciones/notificaciones.module").then(m => m.NotificacionesModule) , canLoad: [LoggedInLazyLoadGuard] },
    { path: "nuevo-meetup/:meetupid", loadChildren: () => import("~/app/nuevo-meet/nuevo-meet.module").then(m => m.NuevoMeetModule) , canLoad: [LoggedInLazyLoadGuard] }


    // { path: "chat/:id/:servicioid", loadChildren: () => import("~/app/chat/chat.module").then(m => m.ServicioChatModule) , canLoad: [LoggedInLazyLoadGuard] }
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})

export class AppRoutingModule { }
