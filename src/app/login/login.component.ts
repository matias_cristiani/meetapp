import { prompt, PromptResult, PromptOptions, inputType, capitalizationType } from "tns-core-modules/ui/dialogs";
import { Component, ElementRef, NgZone, OnInit, AfterViewInit, OnDestroy, ViewChild } from "@angular/core";
import { InternetConnectionService } from "../shared/services/internet-connection.service";
import { Feedback, FeedbackType, FeedbackPosition } from "nativescript-feedback";
import * as appSettings from "tns-core-modules/application-settings";
import { RouterExtensions } from "nativescript-angular/router";
import { ApiService } from "../shared/services/api.service";
import * as appversion from "nativescript-appversion";
import { isIOS } from "tns-core-modules/platform";
import { Page } from "tns-core-modules/ui/page";
import { Color } from "tns-core-modules/color";

declare var IQKeyboardManager;
const CryptoJS = require("crypto-js");

@Component({
    selector: "Login",
    moduleId: module.id,
    templateUrl: "./login.component.html",
    styleUrls: ["./login.component.css"],
    animations: []
})

export class LoginComponent implements OnInit, AfterViewInit, OnDestroy {
    connection$;
    user$;
    
    @ViewChild("email",{ static: false }) email: any;
    @ViewChild("password",{ static: false }) password: any;
    
    public dataUser: any;
    public connectionStatus: boolean;
    public isLoading: boolean;
    public version: string;
    public isPasswordSize: boolean;

    private feedback: Feedback;

    constructor(
        private _internetConnection: InternetConnectionService,
        private _routerExtensions: RouterExtensions,
        private _apiService: ApiService,
        private _ngZone: NgZone,
        private page: Page) {

        this.feedback = new Feedback()
        this.page.actionBarHidden = true;
        this.page.backgroundSpanUnderStatusBar = true;
        this.page.statusBarStyle = "dark";
        this.version = "";
        
        this.isPasswordSize = true;
        this.connectionStatus = true;
        this.isLoading = false;
    }

   ngOnInit(): void {
        this.connection$ = this._internetConnection.connectionStatus$.subscribe(data => {
            this.connectionStatus = data.valueOf();
        });
        
        this.user$ = this._apiService.User$.subscribe(value => {
            this.dataUser = value;
        })

        appversion.getVersionName().then((version) => {
            this.version = version;
            
            appversion.getVersionCode().then((build: string) => {
                      this.version += " - "+ build;
            });
        });


       if (isIOS) {
        let iqKeyboard = IQKeyboardManager.sharedManager();
            iqKeyboard.enable = true;
            iqKeyboard.enableAutoToolbar = true;
            iqKeyboard.shouldResignOnTouchOutside = true;
            iqKeyboard.shouldShowTextFieldPlaceholder = true;
            iqKeyboard.overrideKeyboardAppearance = true;
            iqKeyboard.toolbarDoneBarButtonItemText = "OK";
            iqKeyboard.keyboardDistanceFromTextField = 30;

        }
        
        this.initializeFirebase(1000);
    }

    ngAfterViewInit(): void {
        
        setTimeout(()=>{
            
            if ( appSettings.getString('email') ){
                    this.dataUser.email  =  appSettings.getString('email');

                    if ( appSettings.getString('password') ) {
                        // Encontre email & password grabado.
                        this.dataUser.password =  appSettings.getString('password');
                        return this.loginWithEmail()
                    }else if ( appSettings.getString('token') ){
                        this.initializeFirebase(2000);
                        return this.loginWithEmail()
                    }
            }

        },1500)


        // this.dataUser.email = "demo@meetapp.com";
    }

       
    ngOnDestroy(): void {
        if (this.connection$){ this.connection$.unsubscribe() };
        if (this.user$){ this.user$.unsubscribe() };
    }

    public loginWithEmail() {
        console.dir("@loginWithEmail");
        if(this.isLoading || !this.connectionStatus) return;

        if (this.dataUser.email == '' || !this.dataUser.email) {
           this.showWarning('Debe ingresar email y contraseña, para poder continuar.');
           return;
        }

        if (this.dataUser.password == '' || !this.dataUser.password) {
               this.showWarning('Debe ingresar email y contraseña, para poder continuar.');
               return;
        }
          
        this.isLoading = true;

        let encrypted = CryptoJS.SHA256(this.dataUser.password).toString();
        
        console.dir(encrypted);

        this._apiService.apiForLogin(this.dataUser.email, encrypted, false).then((res: any)=>{
            
            if (res.status != 'success'){
                this.isLoading = false;
                this.showWarning(res.message);
                return;
            }


            this.refreshDataAndToken(res.data);
            
            setTimeout(()=>{
                this.isLoading = false;
                this.navigateTo("home");
            },1000)

        })
    }

    public loginWithGoogle() {
        console.dir("@loginWithGoogle");
        if(this.isLoading || !this.connectionStatus) return;
        
        const firebase = require("nativescript-plugin-firebase");
        let email, password, type, uid, token = null
        
        this.isLoading = true;

        firebase.login({
            type: firebase.LoginType.GOOGLE,
            // Optional 
            googleOptions: {
              hostedDomain: "mygsuitedomain.com",
              // NOTE: no need to add 'profile' nor 'email', because they are always provided
              // NOTE 2: requesting scopes means you may access those properties, but they are not automatically fetched by the plugin
              scopes: [
                "https://www.googleapis.com/auth/contacts.readonly",
                "https://www.googleapis.com/auth/user.addresses.read",
                "https://www.googleapis.com/auth/user.phonenumbers.read",
                "https://www.googleapis.com/auth/userinfo.profile"
              ]
            }

        }).then( (result) => {
                console.dir("result socialGoogle");
                console.dir(result);
         
                //get uid of google
                uid = result.uid;

                //get email of google
                email = result.email;
                
                //get token of google
                result.providers.forEach((p)=>{
                    if(p.id == "google.com"){
                        type = p.id;
                        token = p.token
                    }
                })

                this._apiService.apiForLogin(email, "", true).then((res: any)=>{
                    
                    if (res.status != 'success'){
                        this.isLoading = false;
                        this.showWarning(res.message);
                        return;
                    }

                    this.refreshDataAndToken(String(res.data.id));
                    
                    setTimeout(()=>{
                        this.isLoading = false;
                        this.navigateTo("home");
                    },1000)

                })

        }, (errorMessage) => {
                setTimeout(()=>{ this.isLoading = false;}, 400) 

        });
    }

    public loginWithFacebook() {
        console.dir("@LoginWithFacebook");
        if(this.isLoading || !this.connectionStatus) return;
        
        const firebase = require("nativescript-plugin-firebase");
        let email, password, type, uid, token = null;
        
        this.isLoading = true;

        firebase.login({
            type: firebase.LoginType.FACEBOOK,
            // Optional
            facebookOptions: {
              // defaults to ['public_profile', 'email']
              scopes: ['public_profile', 'email'] // note: this property was renamed from "scope" in 8.4.0
            }
        }).then((result) => {

                //get uid of facebook
                uid = result.uid;

                //get email of facebook
                email = result.email;
                
                //get token of facebook
                result.providers.forEach((p)=>{
                    if(p.id == "facebook.com"){
                        type = p.id;
                        token = p.token
                    }
                })

                this._apiService.apiForLogin(email, "", true).then((res: any)=>{
                    
                    if (res.status != 'success'){
                        this.isLoading = false;
                        this.showWarning(res.message);
                        return;
                    }

                    this.refreshDataAndToken(String(res.data.id));
                    
                    setTimeout(()=>{
                        this.isLoading = false;
                        this.navigateTo("home");
                    },1000)
                })
        }, (errorMessage) => { setTimeout(()=>{ this.isLoading = false;}, 400)  });
    }

    public showRestoreAccount() { 
        if(this.isLoading || !this.connectionStatus) return;

        let options: any = {
                title: "RECUPERAR MI CUENTA",
                defaultText: this.dataUser.email,
                message: "Para reestablecer tu contraseña deberas ingresar tu email a continuacion.",
                okButtonText: "  OK",
                cancelButtonText: "VOLVER  ",
                cancelable: true,
                inputType: inputType.email, // email, number, text, password, or email
                capitalizationType: capitalizationType.none // all. none, sentences or words
            };

            prompt(options).then((result: PromptResult) => {
                console.dir(result)
                if (result.result) {
                    this.submitRestoreAccount(result.text);
                }
            });

    }

    public submitRestoreAccount(email){
        console.dir('@submitRestoreAccount');
        this.isLoading = true;

    }    

    /* feedbacks */
    private showWarning(message){
        this.feedback.warning({
              message: message,
              position: FeedbackPosition.Top,
              type: FeedbackType.Warning,
              backgroundColor: new Color("#df4a32"),
        });
    }


    private showSuccess(message){
        this.feedback.warning({
              message: message,
              position: FeedbackPosition.Top,
              type: FeedbackType.Success,
              backgroundColor: new Color("#95ba65"),
        });
    }

    /* preferences-app */
    
    initializePreferences(authorization){
        console.dir("@initializePreferences");

        if ( appSettings.getString('mapbox_key')) {
            return;
        }
        
    }

    
    /* firebase-push */
    initializeFirebase(timeout){
        setTimeout(()=>{
            const firebase = require("nativescript-plugin-firebase");

            firebase.init({
                   showNotifications: true,
                   showNotificationsWhenInForeground: true,

                   onPushTokenReceivedCallback: function(token) {
                      console.log("Firebase push token: " + token);
                      // alert(token);
                    },

                   onMessageReceivedCallback: (message: any) => {
                      console.log(`Title: ${message.title}`);
                      console.log(`Body: ${message.body}`);
                    }
                 }).then( (instance) => {
                       console.log("firebase.init done");
                       this.subscribeTokenAndTopics();
                 }, (error) => {
                       console.log("firebase.init error: " + error);
                       this.subscribeTokenAndTopics();
                 });
        }, timeout);
    }

    /* Firebase FCM */
    private subscribeTokenAndTopics(){
        const firebase = require("nativescript-plugin-firebase");
        
        let subscripted = appSettings.getBoolean('subscripted') ? appSettings.getBoolean('subscripted') : null;
        let user_id = appSettings.getString('user_id') ? appSettings.getString('user_id') : null;
        let token = appSettings.getString('token') ? appSettings.getString('token') : null;
        
        if (!subscripted) {
            firebase.subscribeToTopic("user").then(() => {
                console.log("Subscribed to user")
                appSettings.setBoolean('subscripted', true);
            });            
        }
                                         
        if (!subscripted && user_id) {
            firebase.subscribeToTopic("user_"+user_id).then(() => {
                console.log("Subscribed to user_"+user_id)
            });
        }
        
        if (!subscripted && !token) {
            firebase.getCurrentPushToken().then(tkn => {
                    if (tkn) {
                        this.dataUser.notification_token = tkn;
                        appSettings.setString("token",tkn);
                    }
            }).catch(err => console.log("Error in doGetCurrentPushToken: " + err));
        }

    }

    /* Refresh data */

    public refreshDataAndToken(user){
        console.dir("@refreshDataAndToken");
        console.dir(user.id);

        //set params appSetting
        appSettings.setString('user_id',String(user.id))       
        appSettings.setString('email', user.email)
        appSettings.setString('password', this.dataUser.password)   
        
        //set observable
        this._apiService.setUser(user);
        
        //set topics for push
        const firebase = require("nativescript-plugin-firebase");
        firebase.subscribeToTopic("user_"+user.id).then(() => console.log("Subscribed to user_"+user.id));
    }
    
    /* Password's */

    public writePassword(args) {
        let textfield = args.object;
        let value = textfield.text;
        let size = value.length;
        
        if (size == 6) {
            this.isPasswordSize = true;
            return;
        }

        if (size > 1 && size < 6) {
            this.isPasswordSize = false;
        }
    }

    public toggleShow(args) {
        let label = args.object;

        this.password.nativeElement.secure = !this.password.nativeElement.secure;
        this.password.nativeElement.class = "form-input";

        label.color = (this.password.nativeElement.secure) ? '#dadada' : '#674ea7';
    }
    
    /* Navigation's */
    
    public navigateTo(page) {
        console.dir('@navigateTo', page);
        if(this.isLoading || !this.connectionStatus) return;
        this._ngZone.run(() => {
            this._routerExtensions.navigate([page], {
                    clearHistory: true,
                    animated: true,
                    transition: {
                        name: "slideLeft",
                        duration: 350,
                        curve: "ease"
                    }
            })
        })
    }

}